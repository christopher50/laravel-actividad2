<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanillaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planillas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_usuario');
            $table->string('Nombre');
            $table->string('Apellido');
            $table->string('password_usuario');
            $table->decimal('Sueldo_base');
            $table->decimal('ISSS');
            $table->decimal('AFP');
            $table->decimal('Renta');
            $table->decimal('Total_descuento');
            $table->decimal('Sueldo_neto');           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planillas');
    }
}

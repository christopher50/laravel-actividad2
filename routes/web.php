<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
//EJEMLOS  DE USO DE RUTAS

/*
Route::get('/funciona', function () {
    return "ya logramos algo con laravel";
});
Route::get('/post/{id}/{nombre}', function ($id,$nombre) { 
    return "este es el post numero " . $id;
})->where('nombre','[a-zA-Z]+');*/
// EJEMPO CON CONTROLADORES Y RUTA:
//Route::get('/inicio', 'tercercontroller@index');
//EJEMPLO DE CONTROLADORES CON PARAMETROS
//Route::get('/inicio/{id}', 'tercercontroller@index');
/*
Route::get('/inicio', 'paginascontroller@inicio');

Route::get('/dondeestamos', 'paginascontroller@dondeestamos');

Route::get('/quienesomos', 'paginascontroller@quienesomos');*/
//Route::resource("post","tercercontroller");
//USO DE RUTAS, CONTROLLERS,BLADE Y BOOTSTRAP
/*
Route::get('/', 'newcontroller@index');
Route::get('/perfiles', 'newcontroller@create');
Route::get('/perfiles_boostrap', 'newcontroller@store');
Route::get('/perfiles3', 'newcontroller@show');
Route::get('/contacto', 'newcontroller@contacto');
Route::get('/galeria', 'newcontroller@galeria');*/
//SEgundo trabajo lp
//Route::post('inicio', 'Auth\LoginController@login')->name('inicio');
//TRABAJO LP
Route::get('/', 'newcontroller@index');
Route::get('/planillas', 'newcontroller@contacto');
Route::get('/inicio', 'newcontroller@create');
Route::get('/registro', 'newcontroller@show');
Route::get('/calculos', 'newcontroller@store');

 /*Route::get('/Pplanilla', function(){
    return View::make('create');
 });*/
/*Route::post('/Pplanilla', function(){
    
    $users = App\planilla::where('id_usuario',$user)->get();
      /*DB::table('planillas')->select(
        array("First_name" => Input::get('firstname'),
              "Last_name" => Input::get("lastname"),
              "E-mail" => Input::get("email"),
              "Username" => Input::get("username"),
              "Password" => Input::get("password"),*/
   /* echo "<table  class='table table-dark'>" ;
    echo "<thead>
    <tr>
      <th scope='col'>Nombre</th>
      <th scope='col'>Apellido</th>
      <th scope='col'>Sueldo base</th>
      <th scope='col'>ISSS</th>
      <th scope='col'>AFP</th>
      <th scope='col'>Renta</th>
      <th scope='col'>Total descuentos</th>
      <th scope='col'>Sueldo neto</th>
    </tr> 
    </thead>";
    echo "<tbody>";
    foreach($users as $datos){
        echo "<tr>" . "<th scope='row'>" . $datos->Nombre . "</th><th scope='row'>" . $datos->Apellido . "</th><th scope='row'>" . "$" . $datos->Sueldo_base . "</th><th scope='row'>" . "$" . $datos->ISSS . "</th><th scope='row'>"  . "$" .   $datos->AFP . "</th><th scope='row'>" . "$" . $datos->Renta . "</th><th scope='row'>" . "$" .  $datos->Total_descuento . "</th><th scope='row'>" . "$" . $datos->Sueldo_neto . "</th>" . "</tr>";
    }
    echo  "</tr>";
    echo "</tbody>";
    echo  "</table>";
      
});*/
Route::get('actualizar', 'newcontroller@actualizar');
/*
Route::get('/perfiles','newcontroller@informacion');
//BBD RAW
/*
Route::get('/insertar',function(){
    
    DB::insert("insert into nombres (nombre, apellido, numero) values (?,?,?)", ["Christopher", "Rodriguez", 18]);
});*/
/*
Route::get("/leer", function(){
    
   $nombres=DB::select("SELECT * FROM nombres where id=?", [1]); 
    foreach($nombres as $nombre){
        return "Nombre: " .  $nombre->nombre . " Apellido: " . $nombre->apellido . " Numero de lista: " . $nombre->numero;
    } 
 });

Route::get("/escribir", function(){
           
           $nombres=DB::select("SELECT * FROM nombres where id=?", [2]);
           foreach($nombres as $nombre){
               return "Nombre: " . $nombre->nombre . "Apellido: " . $nombre->apellido . "Numero de lista: " . $nombre->numero;
}
});

Route::get("/leer2", function(){
    
   $nombres=DB::select("SELECT * FROM nombres where id=?", [3]); 
    foreach($nombres as $nombre){
        return "Nombre: " .  $nombre->nombre . " Apellido: " . $nombre->apellido . " Numero de lista: " . $nombre->numero;
    } 
 });

Route::get("/leer3", function(){
    
   $nombres=DB::select("SELECT * FROM nombres where id=?", [4]); 
    foreach($nombres as $nombre){
        return "Nombre: " .  $nombre->nombre . " Apellido: " . $nombre->apellido . " Numero de lista: " . $nombre->numero;
    } 
 });

Route::get("/leer4", function(){
    
   $nombres=DB::select("SELECT * FROM planillas "); 
    foreach($nombres as $nombre){
        return "Nombre: " . $nombre->Nombre . " Apellido: " . $nombre->Apellido . "Sueldo base: " . $nombre>Sueldo_base;// . "ISSS: " . $nombre->ISSS . "AFP: " . $nombre->AFP . "Renta: " . $nombre->Renta . "Total descuento: " . $nombre->Total_descuento . "Sueldo neto: " . $nombre->Sueldo_neto;
    } 
 });


Route::get("/leer", function(){
   $datos=plantilla::all();
    foreach($datos as $dato){
        echo $dato->Nombre;
    }
    
});
*/
Route::get("/leer8", function(){
    $user = App\planilla::all();
    echo "<table  class='table table-dark'>" ;
    echo "<thead>
    <tr>
      <th scope='col'>Nombre</th>
      <th scope='col'>Apellido</th>
      <th scope='col'>Sueldo base</th>
      <th scope='col'>ISSS</th>
      <th scope='col'>AFP</th>
      <th scope='col'>Renta</th>
      <th scope='col'>Total descuentos</th>
      <th scope='col'>Sueldo neto</th>
    </tr> 
    </thead>";
    echo "<tbody>";
    foreach($user as $datos){
        echo "<tr>" . "<th scope='row'>" . $datos->Nombre . "</th><th scope='row'>" . $datos->Apellido . "</th><th scope='row'>" . "$" . $datos->Sueldo_base . "</th><th scope='row'>" . "$" . $datos->ISSS . "</th><th scope='row'>"  . "$" .   $datos->AFP . "</th><th scope='row'>" . "$" . $datos->Renta . "</th><th scope='row'>" . "$" .  $datos->Total_descuento . "</th><th scope='row'>" . "$" . $datos->Sueldo_neto . "</th>" . "</tr>";
    }
     echo  "</tr>";
    echo "</tbody>";
    echo  "</table>";  
});
Route::get('/planilla/{user}', function ($user) { 
    $user = App\planilla::where('id_usuario',$user)->get();
    echo "<table  class='table table-dark'>" ;
    echo "<thead>
    <tr>
      <th scope='col'>Nombre</th>
      <th scope='col'>Apellido</th>
      <th scope='col'>Sueldo base</th>
      <th scope='col'>ISSS</th>
      <th scope='col'>AFP</th>
      <th scope='col'>Renta</th>
      <th scope='col'>Total descuentos</th>
      <th scope='col'>Sueldo neto</th>
    </tr> 
    </thead>";
    echo "<tbody>";
    foreach($user as $datos){
        echo "<tr>" . "<th scope='row'>" . $datos->Nombre . "</th><th scope='row'>" . $datos->Apellido . "</th><th scope='row'>" . "$" . $datos->Sueldo_base . "</th><th scope='row'>" . "$" . $datos->ISSS . "</th><th scope='row'>"  . "$" .   $datos->AFP . "</th><th scope='row'>" . "$" . $datos->Renta . "</th><th scope='row'>" . "$" .  $datos->Total_descuento . "</th><th scope='row'>" . "$" . $datos->Sueldo_neto . "</th>" . "</tr>";
    }
    echo  "</tr>";
    echo "</tbody>";
    echo  "</table>";
})->where('nombre','[a-zA-Z]+');
Route::get("/insertar", function(){
    $agregar = new App\Planilla;
    $agregar->id_usuario='OS';
    $agregar->Nombre='Oswaldo';
 $agregar->Apellido='Solorzano';
    $agregar->password_usuario='123456';
    $agregar->Sueldo_base=525;
    $agregar->ISSS=15.75;
    $agregar->AFP=38.06;
    $agregar->Renta=0;
    $agregar->Total_descuento=53.81;
    $agregar->Sueldo_neto=471.19;
    $agregar->save();
    return "hecho";
});

Route::get("/actualizar", function(){
    $agregar = App\Planilla::find(20);
    $agregar->id_usuario='EM';
    $agregar->Nombre='Edwin';
 $agregar->Apellido='Monge';
    $agregar->password_usuario='123456';
    $agregar->Sueldo_base=500;
    $agregar->ISSS=15;
    $agregar->AFP=35.25;
    $agregar->Renta=0;
    $agregar->Total_descuento=51.25;
    $agregar->Sueldo_neto=448.75;
    $agregar->save();
    return "actualizado";
});
Route::get("/actualizarvarios", function(){
    App\planilla::Where("id",1)->update(["ISSS"=>15]);
    return "actualizado varios registros";
});

Route::get("/borrar", function(){
   $borrar = App\planilla::find(21);
    $borrar->delete();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-10-2018 a las 06:29:41
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `homestead`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(17, '2018_09_19_175450_create_perfiles_table', 1),
(18, '2018_09_29_132709_create_planilla_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nombres`
--

CREATE TABLE `nombres` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planillas`
--

CREATE TABLE `planillas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Sueldo_base` decimal(8,2) NOT NULL,
  `ISSS` decimal(8,2) NOT NULL,
  `AFP` decimal(8,2) NOT NULL,
  `Renta` decimal(8,2) NOT NULL,
  `Total_descuento` decimal(8,2) NOT NULL,
  `Sueldo_neto` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `planillas`
--

INSERT INTO `planillas` (`id`, `id_usuario`, `Nombre`, `Apellido`, `password_usuario`, `Sueldo_base`, `ISSS`, `AFP`, `Renta`, `Total_descuento`, `Sueldo_neto`, `created_at`, `updated_at`) VALUES
(1, 'CR', 'Christopher', 'Rodriguez', '123456', '500.00', '25.50', '36.25', '46.76', '51.25', '716.11', NULL, NULL),
(2, 'IR', 'Isis', 'Rodriguez', '123456', '850.00', '25.50', '61.63', '46.76', '133.89', '716.11', NULL, NULL),
(3, 'BQ', 'Bryan', 'Quintanilla', '123456', '525.00', '15.75', '38.06', '0.00', '53.81', '471.19', NULL, '2018-09-30 15:50:15'),
(4, 'BM', 'Byron', 'Mendez', '123456', '1200.00', '30.00', '87.00', '97.55', '214.55', '985.45', NULL, NULL),
(5, 'DR', 'David', 'Rigoberto', '123456', '425.00', '12.75', '30.81', '0.00', '43.56', '381.44', NULL, '2018-09-30 15:50:15'),
(6, 'AM', 'Axell', 'Montes', '123456', '600.00', '18.00', '43.50', '24.32', '85.82', '514.18', NULL, NULL),
(7, 'DR', 'Dario', 'Rivas', '123456', '1650.00', '30.00', '119.63', '181.02', '330.15', '1319.85', NULL, NULL),
(8, 'MR', 'Marvin', 'Romero', '123456', '3250.00', '30.00', '235.63', '572.45', '838.08', '2411.92', NULL, NULL),
(9, 'MA', 'Mario', 'Aguilar', '123456', '3250.00', '30.00', '235.63', '572.45', '838.08', '2411.92', NULL, NULL),
(10, 'FT', 'Fernando', 'Trejo', '123456', '542.00', '16.27', '39.32', '19.15', '74.74', '467.61', NULL, NULL),
(11, 'EL', 'Eduardo', 'Lara', '123456', '1050.50', '30.00', '76.16', '69.82', '176.00', '874.50', NULL, NULL),
(12, 'EN', 'Eduardo', 'Nuila', '123456', '475.00', '14.25', '34.44', '0.00', '48.69', '426.31', NULL, '2018-09-30 15:50:15'),
(13, 'MH', 'Marcelo', 'Humberto', '123456', '500.00', '15.00', '36.25', '0.00', '51.25', '448.75', NULL, '2018-09-30 15:50:15'),
(14, 'RL', 'Ronald', 'Leon', '123456', '525.00', '15.75', '38.06', '0.00', '53.81', '471.19', NULL, '2018-09-30 15:50:15'),
(15, 'KP', 'Kevin', 'Pineda', '123456', '850.00', '25.50', '61.63', '46.76', '133.89', '716.11', NULL, NULL),
(16, 'VP', 'Victor', 'Peraza', '123456', '1200.00', '30.00', '87.00', '97.55', '214.55', '985.45', NULL, NULL),
(17, 'DR', 'Diego', 'Ramos', '123456', '425.00', '12.75', '30.81', '0.00', '43.56', '381.44', NULL, '2018-09-30 15:50:15'),
(18, 'DV', 'Duglas', 'viana', '123456', '600.00', '18.00', '43.50', '24.32', '85.82', '514.18', NULL, NULL),
(19, 'PO', 'Pablo', 'Osorio', '123456', '1650.00', '30.00', '119.63', '181.02', '330.15', '1319.85', NULL, NULL),
(20, 'EM', 'Edwin', 'Monge', '123456', '500.00', '15.00', '35.25', '0.00', '51.25', '448.75', NULL, '2018-09-30 15:50:15'),
(21, 'OS', 'Oswaldo', 'Solorzano', '123456', '525.00', '15.75', '38.06', '0.00', '53.81', '471.19', '2018-09-30 15:29:07', '2018-09-30 15:50:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nombres`
--
ALTER TABLE `nombres`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `planillas`
--
ALTER TABLE `planillas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `nombres`
--
ALTER TABLE `nombres`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `planillas`
--
ALTER TABLE `planillas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

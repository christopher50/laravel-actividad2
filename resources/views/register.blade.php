@extends("layouts.plantillablade")
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">

<style type="text/css">
html,
body {
    height: 100%;
}
html {
    display: table;
    margin: auto;
}
body {
    display: table-cell;
    vertical-align: middle;
}
.margin {
  margin: 0 !important;
}
</style>
  
</head>

<body class="blue">
  <div id="login-page" class="row">
    <div class="col s12 z-depth-6 card-panel">
      <form class="login-form" action="http://localhost/laravel/laravel/public/calculos" method="post">
         {{ crsf_field() }}
        <div class="row">
          <div class="input-field col s12 center">
            <img src="{{ asset('img/descarga.jpg')}}" alt="" class="responsive-img valign profile-image-login">
            <p class="center login-form-text">Registro para planilla</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="nombre"  name="nombre" placeholder="Nombre" type="text" class="validate"  required> 
          </div>
        </div>
          <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="apellido" name="apellido" placeholder="Apellido" type="text" class="validate"  required>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-editor-attach-money prefix"></i>
            <input id="sueldo" name="sueldo" placeholder="Sueldo base" type="text" class="validate"  required>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" name="password" placeholder="Password" type="password" class="validate"  required>
          </div>
        </div>
          
        <div class="row">
          <div class="input-field col s12">
            <a href="http://localhost/laravel/laravel/public/calculos" class="btn waves-effect waves-light col s12" name="enviar" value="enviar" type="submit">Register Now</a>
          </div>
          <div class="input-field col s12">
            <p class="margin center medium-small sign-up">Already have an account? <a href="http://localhost/laravel/laravel/public/inicio">Login</a></p>
          </div>
        </div>
      </form>
    </div>
  </div>
</body>

</html>
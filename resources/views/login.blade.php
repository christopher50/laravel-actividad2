@extends("layouts.plantillablade")
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login form using Material Design - Demo by W3lessons</title>
  <!-- CORE CSS-->
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">

<style type="text/css">
html,
body {
    height: 100%;
}
html {
    display: table;
    margin: auto;
}
body {
    display: table-cell;
    vertical-align: middle;
}
.margin {
  margin: 0 !important;
}
</style>
  
</head>

<body class="blue">

  <div id="login-page" class="row">
    <div class="col s12 z-depth-6 card-panel">
      <form class="login-form" action="http://localhost/laravel/laravel/public/Pplanilla" method="POST">
          
        <div class="row">
          <div class="input-field col s12 center">
          <img src="{{ asset('img/descarga.jpg' )}}" class="responsive-img valign profile-image-login">
            <p class="center login-form-text">Registro para planilla</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input class="validate" name="user" placeholder="User name" id="user" type="text" required>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" name="password" placeholder="password" type="password" required>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
              
            <a href="http://localhost/laravel/laravel/public/Pplanilla" class="btn waves-effect waves-light col s12">Login</a>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a href="http://localhost/laravel/laravel/public/registro">Register Now!</a></p>
          </div>         
        </div>

      </form>
    </div>
  </div>


</body>

</html>

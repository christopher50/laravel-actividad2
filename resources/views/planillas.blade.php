@extends("layouts.plantillablade")

<?php
 $user = App\planilla::all();
echo "<div class='page-header'>";
echo "<h1><center>Planilla de trabajadores</center></h>";
echo "</div>";
echo "<div class='container'>";
    echo "<table  class='table table-hover table-bordered table-dark'>" ;
    echo "<thead>
    <tr>
      <th scope='col'>Nombre</th>
      <th scope='col'>Apellido</th>
      <th scope='col'>Sueldo base</th>
      <th scope='col'>ISSS</th>
      <th scope='col'>AFP</th>
      <th scope='col'>Renta</th>
      <th scope='col'>Total descuentos</th>
      <th scope='col'>Sueldo neto</th>
    </tr> 
    </thead>";
    echo "<tbody>";     
    foreach($user as $datos){
        echo "<tr>" . "<th scope='row'>" . $datos->Nombre . "</th><th scope='row'>" . $datos->Apellido . "</th><th scope='row'>" . "$" . $datos->Sueldo_base . "</th><th scope='row'>" . "$" . $datos->ISSS . "</th><th scope='row'>"  . "$" .   $datos->AFP . "</th><th scope='row'>" . "$" . $datos->Renta . "</th><th scope='row'>" . "$" .  $datos->Total_descuento . "</th><th scope='row'>" . "$" . $datos->Sueldo_neto . "</th>" . "</tr>";
    }
     echo  "</tr>";
    echo "</tbody>";
    echo  "</table>";  
echo "</div>";
?>

 <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img  src="{{ asset('img/how-to-learn-more-about-laravel.jpg')}}" class="First-slide" width="100%" height="auto" alt="First slide">
            <div class="container">
              <div class="carousel-caption text-left"> 
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img  src="{{ asset('img/LARAVEL.jpg')}}" width="100%" height="auto" class="Second-slide" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img  src="{{ asset('img/laravel-wallpaper-1_2.png')}}" width="100%" height="auto" class="Third-slide" alt="Third slide">
            <div class="container">
              <div class="carousel-caption text-right">
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
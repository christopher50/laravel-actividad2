@extends("layouts.plantillablade")
@section("cabecera")
<link rel="stylesheet" href="{{ asset('css/perfiles.css')}}">
     
@endsection
@section("inforgeneral")

  <div class="jumbotron">
        <div class="container">
          <h1 class="display-3">Laravel</h1>
          <p class="texto">Laravel es un framework PHP de código abierto que intenta aprovechar las ventajas de otros Frameworks y desarrollar con las últimas versiones de PHP. Su filosofía es desarrollar código PHP de forma elegante y simple basado en un modelo MVC(Modelo-Vista-Controlador).</p>
        </div>
      </div>

      <div class="container">
          <br>
          <h1 class="nota">Perfiles de los creadores</h1>
          <br>
        <div class="row">
          <div class="col-xl-4">
               <center><img class="uno" src="{{ asset('img/20170124.jpg')}}"></center>
            <center><h2>Christopher</h2></center>
            <p class="text-justify">Christopher josé Rodríguez Cerritos, Estudiante del colegio Don Bosco, de segundo año seccion C Sistemas informaticos</p><p><a class="btn btn-secondary" href="http://localhost/laravel/laravel/public/leer" role="button">informacion database</a></p>
          </div>
            
          <div class="col-xl-4">
              <center> <img class="uno" src="{{ asset('img/bfc25d3c-74c2-4b92-a1d1-43d0770d2cfe.jpg')}}"></center>
           <center> <h2>Stanley</h2></center>
            <p class="text-justify">Bryan Stanley Quintanilla Caballero, Estudiante del colegio Don Bosco, de segundo año seccion C Sistemas informaticos</p><p><a class="btn btn-secondary" href="http://localhost/laravel/laravel/public/leer3" role="button">informacion database</a></p>
          </div>
          <div class="col-xl-4">
              <center> <img class="uno" src="{{ asset('img/af838dff-b9b5-4ee2-8389-90f27c3b6678.jpg')}}"></center>
            <center><h2>Axell</h2></center>
            <p class="text-justify">Emmanuel Axell Montes Terezon, Estudiante del colegio Don Bosco, de segundo año seccion C Sistemas informaticos</p><p><a class="btn btn-secondary" href="http://localhost/laravel/laravel/public/leer2" role="button">informacion database</a></p>
          </div>
            </div>
          <div class="row">
              <div class="col-md-2">
              </div>
            <div class="col-md-4">
                 <center><img class="uno" src="{{ asset('img/a3a03ad6-5ecd-4fe8-8ab2-2828c65b89ce.jpg')}}"></center>
            <center><h2>David</h2></center>
            <p class="text-justify">David Rigoberto Tejada Martinez, Estudiante del colegio Don Bosco, de segundo año seccion C Sistemas informaticos</p><p><a class="btn btn-secondary" href="http://localhost/laravel/laravel/public/eescribir" role="button">informacion database</a></p>
          </div>
            <div class="col-md-4">
                <center><img class="uno" src="{{ asset('img/41840667_227167244823252_6142939479763058688_n.jpg')}}"></center>
            <center><h2>Marvin</h2></center>
            <p class="text-justify">Marvin Benjamin Romero Diaz, Estudiante del colegio Don Bosco, de segundo año seccion C Sistemas informaticos</p><p><a class="btn btn-secondary" href="http://localhost/laravel/laravel/public/leer4" role="button">informacion database</a></p>
          </div>
              <div class="col-md-2">
              </div>
          </div>
@endsection
@section("pie")
         
@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planilla extends Model
{
    protected $table = 'planillas';
    protected $fillable = ['id','id_usuario','Nombre','Apellido','password_usuario','Sueldo_base','ISSS','AFP','Renta','Total_descuento','Sueldo_neto'];
    
   /* const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';*/
    
    public function planilla()
    {
        return $this->belongsToMany('App\planilla', 'id')
                    ->withPivot('create', 'read','update', 'delete');
    }
}

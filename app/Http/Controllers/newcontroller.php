<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use Collective\Html\HtmlServiceProvider;
use register;
use calculo;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
 

class newcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("welcome");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       // return input::flashOnly('user','password');
            $user = Input::get('user');

     return view("login");
    }
 public function contacto()
    {
     return view("planillas");
    }
    /*public function galeria()
    {
        $alumnos=["chris","memer","moly","manita"];
        // $alumnos=[];
     return view("contacto", compact("alumnos"));
    }*/
    //TRABAJO PARA LOS PERFILES
public function informacion()
    {
       
     return view("informacion");
       
    }
    public function actualizar()
    {
       
     return view("actualizar");
       
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()// Request $request)
    {
       // return input::flash();
       return view("calculo");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show( Request $request)//$id)
    {
       return view("register");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($dato)
    {
         $user = Input::get('user');

        return view("planillapersonal");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
 //establecemos restful a true
 public $restful = true;
 
 //al hacer uso de get le decimos a laravel que queremos crear una ruta, 
 //cargar una vista etc
 public function get_index()
 {
 
 //si se ha iniciado sesión no dejamos volver
 if(Auth::user())
 {
 return Redirect::to('planillas');
 }
 //mostramos la vista views/login/index.blade.php pasando un título
 return View::make('login.index')->with('title','Login');
 
 }
 
 //anteponemos post al nombre de la función, esto es así porque es la función
 //que recibirá datos por post
 public function post_index()
 {
 
 //recogemos los campos del formulario y los guardamos en un array
 //para pasarselo al método Auth::attempt
 $userdata = array(
 
 'username' => Input::get('user'),
 'password'=> Input::get('password')
 
 ); 
 
 //si los datos son correctos y existe un usuario con los mismos se inicia sesión
 //y redirigimos a la home
 if(Auth::attempt($userdata, true))
 {
 
 return Redirect::to('welcome');
 
 }else{
 //en caso contrario mostramos un error
 return Redirect::to('inicio')->with('error_login', true);
 
 }
 
 }
 
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class paginascontroller extends Controller
{
    public function inicio(){
        
        return view('welcome');
    }
    public function quienesomos(){
        
        return view('quienesomos');
    }
    public function dondeestamos(){
        
        return view('dondeestamos');
    }
}
